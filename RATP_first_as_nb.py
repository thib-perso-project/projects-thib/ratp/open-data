
#%%
import local
import general
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

#%% [markdown]
# # First dataset exploration

#%%
# Choosing file to explore
csv_name = 'qualite-de-lair-mesuree-dans-la-station-auber.csv'
dataset_folder = 'RATP\\'
full_path = local.datasets_path + dataset_folder + csv_name


#%%
# Importing data Frame
ratp = pd.read_csv(full_path, sep=';')
ratp.head()


#%%
#Making a first cleaning part
df = ratp.copy()
short = lambda x:x[:-6]
df['time'] = df['DATE/HEURE'].apply(short).apply(general.t.format_to_df_time)
del(df['DATE/HEURE'])
df.set_index('time', inplace=True)
df.head()


#%%
# Displaying global information
df.info()


#%%
# Displaying overview of values
df.describe()


#%%
# Having abasic plot of each indicator
for i in df.columns:
    df[i].hist()
    plt.title(i)
    plt.show()


#%%
a = 2


#%%



