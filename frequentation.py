# Making all necessary imports
import numpy as np
import pandas as pd
import seaborn as sns
import pandas_profiling

# Plouk plouk

# Add a cell to not display future warnings
import warnings
warnings.filterwarnings("ignore")


# Load
dataset_folder = 'Datasets/'
csv_file = 'trafic-annuel-entrant-par-station-du-reseau-ferre-2016.csv'
csv_location = dataset_folder + csv_file
freq = pd.read_csv(csv_location, sep = ';')
metro = freq[freq['Réseau']=='Métro']

# Clean
metro.replace('3bis', 3.5, inplace=True)
metro.replace('7bis', 7.5, inplace=True)
metro.replace('A', 0, inplace=True)

for i in range(4):
    col = 'Correspondance_' + str(i +1)
    metro[col] = pd.to_numeric(metro[col])
	
	
def get_lines_name():
    lines = np.array([float(i) for i in metro['Correspondance_1'].unique()])
    lines.sort(axis=0)
    return lines
    

def stations_on_line(line):
    stations = []
    for i in range(4):
        col = 'Correspondance_' + str(i +1)
        stations += list(metro[metro[col]==float(line)]['Station'].unique())
    return stations

def stations_per_line(line):
    return len(stations_on_line(line))

def nb_people_per_station(station):
    
    # Call by name
    if type(station)==str:
        if station in metro['Station'].unique():
            people = int(metro[metro['Station'] == station]['Trafic'].tolist()[0])
        else:
            print('Station does not exist, check name.')
            people = 0
    
    # Call by index
    if type(station)==int:
        if station in metro.index:
            
            people = int(metro.loc[station]['Trafic'])
        else:
            print('Station does not exist, check number.')
            people = 0
            
    return people


def nb_people_per_line(line):
    list_of_stations = stations_on_line(line)
    people_line = 0
    for station in list_of_stations:
        people_line += nb_people_per_station(station)
    return people_line

    


def get_station_intersection(station, line=None):
    return None


table = []
max_len = 0
for line in get_lines_name():
    row = []
    for station in stations_on_line(line):
        row.append(nb_people_per_station(station))
    if len(row)>max_len:
        max_len = len(row)
    
    row = np.array(row)
    row.sort()
    row = list(row)
    table.append(row)
    
    
for i in table:
    if len(i)< max_len:
        i += [40000000 for j in range(max_len-len(i))]

sns.heatmap(table, cmap="YlGnBu")
sns.heatmap(table)